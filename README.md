# README #

К проекту подключены 2 библиотеки посредством submodule, т.ч. перед началом сборки нужно выполнить команды

git submodule init

git submodule update

Для сборки необходим [boost](http://www.boost.org) не ниже 1.58 с собранными библиотеками system, thread, date_time, regex, serialization, filesystem

Для сборки используется [cmake](http://www.cmake.org) не ниже 3.2 

Для сборки не в *nix системах нужно указать путь до boost через параметр -DBOOST_ROOT=<Путь до boost>

Собранный сервер это cmdserver

Консольный клиент это cmdclient