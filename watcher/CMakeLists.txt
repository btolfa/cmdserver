# Test for minimum required CMake version 2.8.12
cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)
project(watcher)

set(HEADERS
    FilenameWatchListener.h
    DirectoryWatchListener.h
)

set(SOURCES
    WatchListener.cpp
    ${HEADERS}
)

add_library(${PROJECT_NAME} STATIC ${SOURCES})