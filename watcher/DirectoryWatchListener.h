/** @brief Класс наблюдающий за директориями
* @author Tengiz Sharafiev <btolfa@gmail.com>
* @date 12.05.2015
*/

#pragma once

#include <FileWatcher/FileWatcher.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../server/CommandOutput.h"

namespace fs = boost::filesystem;

namespace cmdserver {

class DirectoryWatchListener : public FW::FileWatchListener {
public:
    DirectoryWatchListener(std::shared_ptr<CommandOutput> const &p_output, FW::FileWatcher &service,
                           fs::path const &path)
            : p_output_(p_output), service_(service), path_(path) {
        id_ = service_.addWatch(path_.string(), this);
    }

    ~DirectoryWatchListener() {
        service_.removeWatch(id_);
    }

    void handleFileAction(FW::WatchID, FW::String const&, const FW::String &filename,
                          FW::Action action) override {
        if (auto p_output = p_output_.lock()) {
            std::string desc;
            switch (action) {
                case FW::Action::Add:
                    desc = "added";
                    break;
                case FW::Action::Delete:
                    desc = "deleted";
                    break;
                case FW::Action::Modified:
                    desc = "modified";
                    break;
                default:
                    desc = "some mistery with";
                    break;
            }
            p_output->write("At " + path_.string() + " " + desc + " " + filename + "\n");
        }
    }

private:
    std::weak_ptr<CommandOutput> p_output_;
    FW::FileWatcher &service_;
    FW::WatchID id_;
    fs::path path_;
};


}



