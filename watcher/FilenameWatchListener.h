/** @brief Класс наблюдающий за файлами
* @author Tengiz Sharafiev <btolfa@gmail.com>
* @date 12.05.2015
*/

#pragma once

#include <FileWatcher/FileWatcher.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

namespace cmdserver {

class FilenameWatchListener : public FW::FileWatchListener {

public:
    FilenameWatchListener(std::shared_ptr<CommandOutput> const &p_output, FW::FileWatcher &service,
            fs::path const &path)
    : p_output_(p_output), service_(service), path_(fs::absolute(path)) {
        id_ = service_.addWatch(path_.parent_path().string(), this);
    }

    ~FilenameWatchListener() {
        service_.removeWatch(id_);
    }

    void handleFileAction(FW::WatchID , const FW::String &, const FW::String &filename,
                                  FW::Action action) override {
        if (auto p_output = p_output_.lock()) {
            if (path_.filename() == filename) {
                std::string desc;
                switch (action) {
                    case FW::Action::Add:
                        desc = "added";
                        break;
                    case FW::Action::Delete:
                        desc = "deleted";
                        break;
                    case FW::Action::Modified:
                        desc = "modified";
                        break;
                    default:
                        desc = "some mistery with";
                        break;
                }
                p_output->write(filename + " is " + desc  + "\n");
            }
        }
    }

private:
    std::weak_ptr<CommandOutput> p_output_;
    FW::FileWatcher &service_;
    FW::WatchID id_;
    fs::path path_;
};

}



