/** @brief Интерфейс с помощью которого команды будут выводить ответы
 *
 */

#pragma once
#ifndef CMDSERVER_COMMANDOUTPUT_H
#define CMDSERVER_COMMANDOUTPUT_H

#include <string>
#include <boost/filesystem.hpp>

namespace cmdserver {

class CommandOutput {
public:
    virtual ~CommandOutput() = default;

    virtual void write(std::string const& output) = 0;
    virtual void add_record(const boost::filesystem::path &path) = 0;
    virtual void remove_record(const boost::filesystem::path &path) = 0;
};

}

#endif //CMDSERVER_COMMANDOUTPUT_H
