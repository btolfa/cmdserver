/** Класс реализующий соединение с сервером
 *
 * @author Tengiz Sharafiev <btolfa@gmail.com>
 */

#pragma once
#ifndef CMDSERVER_TCPCONNECTION_H
#define CMDSERVER_TCPCONNECTION_H

#include <memory>
#include <unordered_map>
#include <iostream>

#include <boost/asio.hpp>
#include <FileWatcher/FileWatcher.h>
#include "CommandOutput.h"
#include "../watcher/DirectoryWatchListener.h"
#include "../watcher/FilenameWatchListener.h"
#include "../cmdparser/CmdParser.h"

namespace fs = boost::filesystem;

namespace asio = boost::asio;
using boost::asio::ip::tcp;

namespace cmdserver {

class TcpConnection : public std::enable_shared_from_this<TcpConnection>, public CommandOutput {
public:
    TcpConnection(asio::io_service & ios, FW::FileWatcher &service) : socket_(ios), service_(service) {}

    tcp::socket& socket() {
        return socket_;
    }

    void start() {
        asio::async_read_until(socket_, readbuf_, '\n',
            [cn = shared_from_this()](const boost::system::error_code& ec, std::size_t){
            if (! ec) {
                // вытаскиваем данные из буфера
                auto bufs = cn->readbuf_.data();
                std::string message(asio::buffers_begin(bufs), asio::buffers_begin(bufs) + cn->readbuf_.size() - 1);
                cn->readbuf_.consume(cn->readbuf_.size());

                std::cout << "[TcpConnection] Message received: " << message << std::endl;
                try {
                    // Парсим команду
                    auto p_functor = CmdParser::parse(message);
                    (*p_functor)(cn);
                } catch (ParserException const& exception) {
                    std::string error_msg = std::string{"Error: "} + exception.what() + "\n";
                    std::cout << "[TcpConnection] " << error_msg;
                    cn->write(error_msg);
                };

                // Читаем ещё раз
                cn->start();
            }
        });
    }


    void write(std::string const& output) override {
        std::ostream os(&writebuf_);
        os << output;

        std::cout << "[TcpConnection] " << "Send message:\n" << output;

        asio::async_write(socket_, writebuf_, [cn = shared_from_this()](const boost::system::error_code&, std::size_t){});
    }

    void add_record(const boost::filesystem::path &path) override {
        auto cpath = fs::canonical(path);

        if (! registry_.count(cpath.string())) {
            // Если этот путь ранее не был зарегистрирован
            if (fs::exists(cpath)) {
                // Путь существует
                if (fs::is_directory(cpath)) {
                    registry_.emplace(cpath.string(), std::make_unique<DirectoryWatchListener>(shared_from_this(), service_, cpath));
                    watching_message(cpath);
                } else {
                    registry_.emplace(cpath.string(), std::make_unique<FilenameWatchListener>(shared_from_this(), service_, cpath));
                    watching_message(cpath);
                }
            } else {
                // Путь не существует, и таки попали сюда, значит это файл
                registry_.emplace(cpath.string(), std::make_unique<FilenameWatchListener>(shared_from_this(), service_, cpath));
                watching_message(cpath);
            }
        }
    }

    void watching_message(const boost::filesystem::path &path) {
        std::cout << "[TcpConnection] Watching " << path.string() << std::endl;
        write("Watching " + path.string() + "\n");
    }

    void remove_record(const boost::filesystem::path &path) override {
        auto cpath = fs::canonical(path);
        auto it = registry_.find(cpath.string());
        if (it != registry_.end()) {
            registry_.erase(it);
            std::cout << "[TcpConnection] Unwatching " << cpath.string() << std::endl;
            write("Unwatching " + cpath.string() + "\n");
        }
    }

private:
    tcp::socket socket_;
    FW::FileWatcher &service_;

    asio::streambuf readbuf_;
    asio::streambuf writebuf_;

    std::unordered_map<std::string, std::unique_ptr<FW::FileWatchListener>> registry_;
};

}

#endif //CMDSERVER_TCPCONNECTION_H
