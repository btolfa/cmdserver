/** Класс реализующий сервер
 *
 * @author Tengiz Sharafiev <btolfa@gmail.com>
 */

#pragma once
#ifndef CMDSERVER_TCPSERVER_H
#define CMDSERVER_TCPSERVER_H

#include <iostream>
#include <boost/asio.hpp>

#include "TcpConnection.h"

namespace asio = boost::asio;
using boost::asio::ip::tcp;

namespace cmdserver {

class TcpServer {
public:
    TcpServer(asio::io_service &ios, const std::uint16_t port)
            : acceptor_(ios, tcp::endpoint(tcp::v4(), port)), timer_(ios) {
        start_accept();
        do_update();
    }
private:
    void start_accept() {
        auto p_connection = std::make_shared<TcpConnection>(acceptor_.get_io_service(), service_);
        acceptor_.async_accept(p_connection->socket(), [this, p_connection](const boost::system::error_code& error){
            on_accept(p_connection, error);
        });
        std::clog << "[TcpServer] " << "Waiting for another client connection\n";
    }

    void on_accept(std::shared_ptr<TcpConnection> p_connection, const boost::system::error_code& error) {
        if (!error) {
            std::clog << "[TcpServer] "<< "New connection from " << p_connection->socket().remote_endpoint().address().to_string() << "\n";
            p_connection->start();
        }

        start_accept();
    }

    void do_update() {
        timer_.expires_from_now(boost::posix_time::seconds{1});
        timer_.async_wait([this](const boost::system::error_code& ec){
            if (ec != asio::error::basic_errors::operation_aborted) {
                service_.update();
                do_update();
            }
        });
    }

private:
    tcp::acceptor acceptor_;
    FW::FileWatcher service_;
    asio::deadline_timer timer_;
};

}

#endif //CMDSERVER_TCPSERVER_H
