/** Класс реализующий клиент
 *
 * @author Tengiz Sharafiev <btolfa@gmail.com>
 */

#pragma once
#ifndef CMDSERVER_TCPCLIENT_H
#define CMDSERVER_TCPCLIENT_H

#include <iostream>
#include <chrono>
#include <memory>
#include <string>

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/signals2.hpp>

namespace cmdserver {

namespace asio = boost::asio;
using boost::asio::ip::tcp;

class TcpClient {
public:
    using connect_signal_t = boost::signals2::signal<void()>;

    TcpClient(asio::io_service& ios, std::string host, std::uint16_t port)
            : resolver_(ios), reconnect_timer_(ios),
              host_(std::move(host)), port_(port) {
    }

    /** @brief Отправляем строку удалённому серверу
     *
     */
    void send(std::string const& message) {
        resolver_.get_io_service().dispatch([this, message]{
            std::ostream os(&writebuf_);
            os << message << '\n';
            asio::async_write(*p_socket_, writebuf_, [](const boost::system::error_code&, std::size_t bt){
                std::cout << "[TcpClient] message with " << bt << " bytes sent" << std::endl;
            });
        });
    }

    /** @brief регистрация обработчика который будет вызван сразу после успешного подключения
     *
     */
    boost::signals2::connection register_on_connect(connect_signal_t::slot_type slot) {
        return connect_signal_.connect(slot);
    }

    void do_connect() {
        std::clog << "[TcpClient] "<< "Connecting to " << host_ << ":" << port_ << "\n";

        // Получаем ip адрес из доменного имени
        tcp::resolver::query query{host_, std::to_string(port_)};

        resolver_.async_resolve(query, [this](const boost::system::error_code& ec, asio::ip::tcp::resolver::iterator it){
            if (! ec) {
                // Создаём чистый объект socket перед началом очередной попытки подключиться
                p_socket_ = std::make_unique<tcp::socket>(resolver_.get_io_service());

                p_socket_->async_connect(*it, [this](const boost::system::error_code& ecc){
                    if (! ecc) {
                        connect_signal_();
                        do_read();
                    } else {
                        reconnect_with_pause();
                    }
                });
            } else {
                std::clog << "[TcpClient] " << "Failed to resolve " << host_ << ":" << port_ << " - "
                    << ec.message() << "\n";
                reconnect_with_pause();
            }
        });
    }

    void do_read() {
        asio::async_read_until(*p_socket_, readbuf_, '\n', [this](const boost::system::error_code& ec, std::size_t){
            if (! ec) {
                // вытаскиваем данные из буфера
                auto bufs = readbuf_.data();
                std::string message(asio::buffers_begin(bufs), asio::buffers_begin(bufs) + readbuf_.size() - 1);
                readbuf_.consume(readbuf_.size());

                std::cout << message << std::endl;

                do_read();
            }
        });
    }

private:
    /** @brief Пытаемся подключиться повторно через 5 секунд
     *
     */
    void reconnect_with_pause() {
        std::clog << "[TcpClient] " << "Pause 5 sec\n";

        reconnect_timer_.expires_from_now(boost::posix_time::seconds(5));
        reconnect_timer_.async_wait([this](const boost::system::error_code& ecc){
            if (ecc != asio::error::operation_aborted) {
                do_connect();
            }
        });
    }

private:
    std::unique_ptr<tcp::socket> p_socket_;
    tcp::resolver resolver_;
    asio::deadline_timer reconnect_timer_;

    std::string host_;
    std::uint16_t port_;

    asio::streambuf writebuf_;
    asio::streambuf readbuf_;

    connect_signal_t connect_signal_;
};

}

#endif //CMDSERVER_TCPCLIENT_H
