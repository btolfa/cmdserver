/** Точка входа в приложение сервер
* @author Tengiz Sharafiev <btolfa@gmail.com>
* @date 14.05.2015
*/

#include <boost/asio.hpp>

#include "server/TcpServer.h"

int main() {
    boost::asio::io_service ios;
    std::uint16_t port = 20000;

    cmdserver::TcpServer server{ios, port};
    ios.run();

    return 0;
}