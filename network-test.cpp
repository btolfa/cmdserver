#include <boost/asio.hpp>

#include "server/TcpServer.h"
#include "client/TcpClient.h"

int main() {

    boost::asio::io_service ios;
    std::uint16_t port = 20000;

    cmdserver::TcpServer server{ios, port};
    cmdserver::TcpClient client{ios, "127.0.0.1", port};
    client.register_on_connect([&ios]{
        std::cout << std::endl;
        ios.stop();
    });
    client.do_connect();

    ios.run();

    return 0;
}