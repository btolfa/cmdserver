# Test for minimum required CMake version 2.8.12
cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)

project(tests)

# Enable ExternalProject CMake module
include(ExternalProject)

# Set default ExternalProject root directory
set_directory_properties(PROPERTIES EP_PREFIX ${CMAKE_BINARY_DIR}/ThirdParty)

if(THREADS_FOUND)
    set(DISABLE_PTHREADS "-Dgtest_disable_pthreads=OFF")
else()
    set(DISABLE_PTHREADS "-Dgtest_disable_pthreads=ON")
endif()

# Add gmock
ExternalProject_Add(
    googlemock
    SVN_REPOSITORY http://googlemock.googlecode.com/svn/trunk/
    SVN_REVISION -r 507
    TIMEOUT 10
    UPDATE_COMMAND ""
    # Force separate output paths for debug and release builds to allow easy
    # identification of correct lib in subsequent TARGET_LINK_LIBRARIES commands
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG:PATH=DebugLibs
               -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE:PATH=ReleaseLibs
               ${DISABLE_PTHREADS}
    # Disable install step
    INSTALL_COMMAND ""
    # Wrap download, configure and build steps in a script to log output
    LOG_DOWNLOAD ON
    LOG_CONFIGURE ON
    LOG_BUILD ON)

# Specify include dir
ExternalProject_Get_Property(googlemock source_dir)
set(GMOCK_INCLUDE_DIRS ${source_dir}/include)
set(GTEST_INCLUDE_DIRS ${source_dir}/gtest/include)


if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows" AND MINGW)
    set(CMAKE_FIND_LIBRARY_PREFIXES lib)
    set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
endif()

# Specify link libraries
ExternalProject_Get_Property(googlemock binary_dir)
set (GMOCK_BOTH_LIBRARIES
    debug ${binary_dir}/DebugLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gmock.a
    debug ${binary_dir}/DebugLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gmock_main.a
    optimized ${binary_dir}/ReleaseLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gmock.a
    optimized ${binary_dir}/ReleaseLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gmock_main.a)
set (GTEST_BOTH_LIBRARIES
    debug ${binary_dir}/gtest/DebugLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gtest.a
    debug ${binary_dir}/gtest/DebugLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gtest_main.a
    optimized ${binary_dir}/gtest/ReleaseLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gtest.a
    optimized ${binary_dir}/gtest/ReleaseLibs/${CMAKE_FIND_LIBRARY_PREFIXES}gtest_main.a)

include_directories(${GMOCK_INCLUDE_DIRS} ${GTEST_INCLUDE_DIRS})

# cmd-parser-test
add_executable(cmd-parser-test
    CmdParserTest.cpp
    ListCommandTest.cpp
    CatCommandTest.cpp
    FsOperationSetUp.h)
add_dependencies(cmd-parser-test googlemock)
target_link_libraries(cmd-parser-test ${GTEST_BOTH_LIBRARIES} ${GMOCK_BOTH_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} ${Boost_LIBRARIES} cmdparser)

add_test(NAME cmd-parser-test.CmdParserTest
    COMMAND cmd-parser-test --gtest_filter=CmdParserTest*
    WORKING_DIRECTORY .)

add_test(NAME cmd-parser-test.ListCommandTest
    COMMAND cmd-parser-test --gtest_filter=ListCommandTest*
    WORKING_DIRECTORY .)

add_test(NAME cmd-parser-test.CatCommandTest
    COMMAND cmd-parser-test --gtest_filter=CatCommandTest*
    WORKING_DIRECTORY .)

# watcher-test
add_executable(watcher-test
    WatcherTest.cpp
    FsOperationSetUp.h)
add_dependencies(watcher-test googlemock)
target_link_libraries(watcher-test ${GTEST_BOTH_LIBRARIES} ${GMOCK_BOTH_LIBRARIES} ${Boost_LIBRARIES} simplefilewatcher)

add_test(NAME watcher-test
    COMMAND watcher-test
    WORKING_DIRECTORY .)

