/** @brief Тест проверяющий парсинг и исполнение команды ls
 *
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using ::testing::StrEq;

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../cmdparser/ListCommand.h"
#include "FsOperationSetUp.h"
#include "../cmdparser/ParserException.h"

namespace fs = boost::filesystem;

namespace {

class ListCommandTest : public FsOperationSetUp,  public ::testing::Test {};

class MockCommandOutput : public cmdserver::CommandOutput {
public:
    MOCK_METHOD1(write, void(std::string const&));
    void add_record(boost::filesystem::path const&) override {};
    void remove_record(boost::filesystem::path const&) override {};
};

/** @brief Если параметр один файл, должен вывести его имя
 *
 */
TEST_F(ListCommandTest, ShouldPrintOneFile) {
    auto output = std::make_shared<MockCommandOutput>();

    {
        cmdserver::ListCommand cmd("ls t1.txt");
        EXPECT_CALL(*output, write(StrEq("t1.txt\n")));
        cmd(output);
    }

    {
        cmdserver::ListCommand cmd("ls t2/1.txt");
        EXPECT_CALL(*output, write(StrEq("1.txt\n")));
        cmd(output);
    }
}

/** @brief Должен вывести содержимое директории, если пусть это директория
 *
 */
TEST_F(ListCommandTest, ShouldPrintDirectoryList) {
    auto output = std::make_shared<MockCommandOutput>();

    {
        cmdserver::ListCommand cmd("ls .");
        EXPECT_CALL(*output, write(StrEq("t1.txt\nt2\n")));
        cmd(output);
    }

    {
        cmdserver::ListCommand cmd("ls t2");
        EXPECT_CALL(*output, write(StrEq("1.txt\n2.txt\n3.txt\n")));
        cmd(output);
    }
}

class InvalidParameterTest : public ListCommandTest, public ::testing::WithParamInterface<const char *> {};

/** @brief Должен бросить исключение т.к. параметры команды не корректны
 *
 */
TEST_P(InvalidParameterTest, ShouldThrowException) {
    EXPECT_THROW(std::make_unique<cmdserver::ListCommand>(GetParam()), cmdserver::ParserException);
}

INSTANTIATE_TEST_CASE_P(ListCommandTest,
                        InvalidParameterTest,
                        ::testing::Values("ls ", // Нет параметра
                                          "ls t3.txt", // Параметр не существующий файл
                                          "ls ;" // Параметр не путь
                                          ));


class ValidParameterTest : public ListCommandTest, public ::testing::WithParamInterface<const char *> {};

/** @brief Не должен бросать исключение т.к. параметр корректный
 *
 */
TEST_P(ValidParameterTest, ShouldntThrow) {
    EXPECT_NO_THROW(std::make_unique<cmdserver::ListCommand>(GetParam()));
}

INSTANTIATE_TEST_CASE_P(ListCommandTest,
                        ValidParameterTest,
                        ::testing::Values("ls t1.txt",
                                          "ls t2",
                                          "ls t2/1.txt",
                                          "ls /",
                                          "ls ."
                        ));


}

