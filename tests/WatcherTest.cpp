/** @brief Тесты проверяющий возможности SimpleFileWatcher
 *
 *  Судя по описанию библиотека может следить за созданием, удалением и изменением файлов в директории
 *
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using ::testing::StrEq;
using ::testing::Eq;
using ::testing::_;

#include <chrono>
#include <thread>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "FsOperationSetUp.h"
#include "../watcher/DirectoryWatchListener.h"
#include "../watcher/FilenameWatchListener.h"

#include <FileWatcher/FileWatcher.h>

namespace fs = boost::filesystem;

namespace {

class WatcherTest : public FsOperationSetUp, public ::testing::Test {

};

class MockListener : public FW::FileWatchListener {
public:
    MOCK_METHOD4(handleFileAction, void(FW::WatchID watchid, std::string const& dir, std::string const& filename,
            FW::Action action));
};

class MockCommandOutput : public cmdserver::CommandOutput {
public:
    MOCK_METHOD1(write, void(std::string const&));
    void add_record(boost::filesystem::path const& ) override {};
    void remove_record(boost::filesystem::path const& ) override {};
};

/** @brief Должен сигнализировать после создания файла
 *
 */
TEST_F(WatcherTest, ShouldNotifyOnAddEvent) {
    auto p_listener = std::make_unique<MockListener>();
    FW::FileWatcher service;
    auto watchid = service.addWatch(".", p_listener.get());

    // Создаём пустой файл new.txt
    { fs::ofstream f("new.txt"); }

    EXPECT_CALL(*p_listener,
                handleFileAction(Eq(watchid), StrEq("."), StrEq("new.txt"), Eq(FW::Actions::Add)));
    service.update();
}

/** @brief Должен сигнализировать при изменении файла
 *
 */
TEST_F(WatcherTest, ShouldNotifyOnModify) {
    auto p_listener = std::make_unique<MockListener>();
    FW::FileWatcher service;
    auto watchid = service.addWatch(".", p_listener.get());

    {
        // Update file modify time
        fs::ofstream f("t1.txt");
    }

    EXPECT_CALL(*p_listener,
                handleFileAction(Eq(watchid), StrEq("."), StrEq("t1.txt"), Eq(FW::Actions::Modified)));
    service.update();
}

/** @brief Должен сигнализировать при удалении файла
 *
 */
TEST_F(WatcherTest, ShouldNotifyOnDelete) {
    auto p_listener = std::make_unique<MockListener>();
    FW::FileWatcher service;
    auto watchid = service.addWatch(".", p_listener.get());

    fs::remove("t1.txt");

    EXPECT_CALL(*p_listener,
                handleFileAction(Eq(watchid), StrEq("."), StrEq("t1.txt"), Eq(FW::Actions::Delete)));
    service.update();
}


/** @brief Должен уведомлять о событии всех наблюдателей, если следять несколько
 *
 */
TEST_F(WatcherTest, ShouldWatchTheSameDirectory) {
    auto p_listener1 = std::make_unique<MockListener>();
    auto p_listener2 = std::make_unique<MockListener>();
    FW::FileWatcher service;
    auto watchid1 = service.addWatch(".", p_listener1.get());
    auto watchid2 = service.addWatch(".", p_listener2.get());

    { fs::ofstream f("new.txt"); }

    EXPECT_CALL(*p_listener1,
                handleFileAction(Eq(watchid1), StrEq("."), StrEq("new.txt"), Eq(FW::Actions::Add)));
    EXPECT_CALL(*p_listener2,
                handleFileAction(Eq(watchid2), StrEq("."), StrEq("new.txt"), Eq(FW::Actions::Add)));
    service.update();
}

/** @brief Должен напечатать сообщение при создании файла в директории
*/
TEST_F(WatcherTest, ShouldNotifyPrintOnDirectoryAdd) {
    auto p_output = std::make_shared<MockCommandOutput>();
    FW::FileWatcher service;

    {
        cmdserver::DirectoryWatchListener listener{p_output, service, "."};

        {
            fs::ofstream f("new.txt");
        }

        EXPECT_CALL(*p_output, write(StrEq("At . added new.txt\n")));
        service.update();
    }
}

/** @brief Должен напечатать сообщение при удалении файла в директории
*/
TEST_F(WatcherTest, ShouldNotifyPrintOnDirectoryDelete) {
    auto p_output = std::make_shared<MockCommandOutput>();
    FW::FileWatcher service;

    {
        cmdserver::DirectoryWatchListener listener{p_output, service, "."};

        {
            fs::remove("t1.txt");
        }

        EXPECT_CALL(*p_output, write(StrEq("At . deleted t1.txt\n")));
        service.update();
    }
}

/** @brief Должен напечатать сообщение при изменении файла в директории
*/
TEST_F(WatcherTest, ShouldNotifyPrintOnDirectoryModify) {
    auto p_output = std::make_shared<MockCommandOutput>();
    FW::FileWatcher service;

    {
        cmdserver::DirectoryWatchListener listener{p_output, service, "."};

        {
            fs::ofstream f("t1.txt");
            f << "Hell Worl!\n";
        }

        EXPECT_CALL(*p_output, write(StrEq("At . modified t1.txt\n"))).Times(::testing::AnyNumber());
        service.update();
    }
}

/** @brief Должен напечатать сообщение при создании файла
*/
TEST_F(WatcherTest, ShouldNotifyPrintOnFileAdd) {
    auto p_output = std::make_shared<MockCommandOutput>();
    FW::FileWatcher service;

    {
        cmdserver::FilenameWatchListener listener{p_output, service, "new.txt"};

        {
            fs::ofstream f("new.txt");
        }

        EXPECT_CALL(*p_output, write(StrEq("new.txt is added\n")));
        service.update();
    }
}

/** @brief Должен напечатать сообщение при удалении файла
*/
TEST_F(WatcherTest, ShouldNotifyPrintOnFileDelete) {
    auto p_output = std::make_shared<MockCommandOutput>();
    FW::FileWatcher service;

    {
        cmdserver::FilenameWatchListener listener{p_output, service, "t1.txt"};

        {
            fs::remove("t1.txt");
        }

        EXPECT_CALL(*p_output, write(StrEq("t1.txt is deleted\n")));
        service.update();
    }
}

/** @brief Должен напечатать сообщение при изменении файла
*/
TEST_F(WatcherTest, ShouldNotifyPrintOnFileModify) {
    auto p_output = std::make_shared<MockCommandOutput>();
    FW::FileWatcher service;

    {
        cmdserver::FilenameWatchListener listener{p_output, service, "t1.txt"};

        {
            fs::ofstream f("t1.txt");
            f << "Hell Worl!\n";
        }

        EXPECT_CALL(*p_output, write(StrEq("t1.txt is modified\n"))).Times(::testing::AnyNumber());
        service.update();
    }
}


}