/** @brief Тест проверяющий работу парсера входящих команд
 *
 *
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using ::testing::WhenDynamicCastTo;
using ::testing::Pointee;

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "../cmdparser/CmdParser.h"
#include "FsOperationSetUp.h"

namespace fs = boost::filesystem;

namespace {

class CmdParserTest : public FsOperationSetUp, public ::testing::Test {
};

/** @brief Должен вернуть объект для команды ls
 *
 */
TEST_F(CmdParserTest, ShouldReturnListCommand) {
    std::string cmdline = "ls t2";
    auto cmd = cmdserver::CmdParser::parse(cmdline);

    EXPECT_THAT(cmd.get(), WhenDynamicCastTo<cmdserver::ListCommand *>(Pointee(::testing::_)));
}

/** @brief Должен вернуть объект для команды cat
 *
 */
TEST_F(CmdParserTest, ShouldReturnCatCommand) {
    std::string cmdline = "cat t1.txt";
    auto cmd = cmdserver::CmdParser::parse(cmdline);

    EXPECT_THAT(cmd.get(), WhenDynamicCastTo<cmdserver::CatCommand *>(Pointee(::testing::_)));
}

/** @brief Должен вернуть объект для команды watch
 *
 */
TEST_F(CmdParserTest, ShouldReturnWatchCommand) {
    std::string cmdline = "Watch t1.txt";
    auto cmd = cmdserver::CmdParser::parse(cmdline);

    EXPECT_THAT(cmd.get(), WhenDynamicCastTo<cmdserver::WatchCommand *>(Pointee(::testing::_)));
}

/** @brief Должен вернуть объект для команды unwatch
 *
 */
TEST_F(CmdParserTest, ShouldReturnUnwatchCommand) {
    std::string cmdline = "Unwatch t1.txt";
    auto cmd = cmdserver::CmdParser::parse(cmdline);

    EXPECT_THAT(cmd.get(), WhenDynamicCastTo<cmdserver::UnwatchCommand *>(Pointee(::testing::_)));
}

TEST_F(CmdParserTest, ShouldThrowParsingExceptionIfCommandUnknown) {
    std::string cmdline = "rm t1.txt";
    EXPECT_THROW(cmdserver::CmdParser::parse(cmdline), cmdserver::ParserException);
}

}