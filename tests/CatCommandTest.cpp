/** @brief Тест проверяющий работу команды cat
 *
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>

using ::testing::StrEq;

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "FsOperationSetUp.h"
#include "../cmdparser/CatCommand.h"
#include "../cmdparser/ParserException.h"

namespace fs = boost::filesystem;

namespace {

class CatCommandTest : public FsOperationSetUp, public ::testing::Test {
};

class MockCommandOutput : public cmdserver::CommandOutput {
public:
    MOCK_METHOD1(write, void(std::string const&));
    void add_record(boost::filesystem::path const&) override {};
    void remove_record(boost::filesystem::path const&) override {};
};

TEST_F(CatCommandTest, ShouldPrintFileContent) {
    auto output = std::make_shared<MockCommandOutput>();

    {
        cmdserver::CatCommand cmd("cat t1.txt");
        EXPECT_CALL(*output, write(StrEq("Hello World!\n")));
        cmd(output);
    }

    {
        cmdserver::CatCommand cmd("cat t2/1.txt");
        EXPECT_CALL(*output, write(StrEq("Goodbye World!\n")));
        cmd(output);
    }

}

class InvalidParameterTest1 : public CatCommandTest, public ::testing::WithParamInterface<const char *> {};

/** @brief Должен бросить исключение т.к. параметры команды не корректны
 *
 */
TEST_P(InvalidParameterTest1, ShouldThrowException) {
    EXPECT_THROW(std::make_unique<cmdserver::CatCommand>(GetParam()), cmdserver::ParserException);
}

INSTANTIATE_TEST_CASE_P(CatCommandTest,
                        InvalidParameterTest1,
                        ::testing::Values("cat ", // Нет параметра
                                          "cat t3.txt", // Параметр не существующий файл
                                          "cat ;", // Параметр не путь
                                          "cat t2" // Директория
                                          ));


class ValidParameterTest1 : public CatCommandTest, public ::testing::WithParamInterface<const char *> {};

/** @brief Не должен бросать исключение т.к. параметр корректный
 *
 */
TEST_P(ValidParameterTest1, ShouldntThrow) {
    EXPECT_NO_THROW(std::make_unique<cmdserver::CatCommand>(GetParam()));
}

INSTANTIATE_TEST_CASE_P(CatCommandTest,
                        ValidParameterTest1,
                        ::testing::Values("cat t1.txt",
                                          "cat t2/1.txt"
                        ));


}
