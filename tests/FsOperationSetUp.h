/** @brief Базовый класс который готовит файлы для тестов
 *  Директории и файлы которые существуют во время выполенения теста
 * ./t1.txt
 * ./t2/
 * ./t2/1.txt
 * ./t2/2.txt
 * ./t2/3.txt
 */

#pragma once
#ifndef CMDSERVER_FILEOPERATIONBASE_H
#define CMDSERVER_FILEOPERATIONBASE_H

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace fs = boost::filesystem;

class FsOperationSetUp {
public:
    FsOperationSetUp() {
        // Создаём директорию в которой будут проходить все тесты
        testenv = fs::absolute(fs::unique_path());
        fs::create_directory(testenv);
        fs::current_path(testenv);

        // Создаём тестовый файл
        fs::ofstream t1(testenv / "t1.txt");
        t1 << "Hello World!\n";

        // Создаём тестовую директорию
        fs::create_directory(testenv / "t2");
        fs::ofstream f1(testenv / "t2" / "1.txt");
        f1 << "Goodbye World!\n";
        fs::ofstream f2(testenv / "t2" / "2.txt");
        fs::ofstream f3(testenv / "t2" / "3.txt");
    }

    ~FsOperationSetUp() {
        fs::current_path(testenv.parent_path());
        fs::remove_all(testenv);
    }
protected:
    fs::path testenv;
};

#endif //CMDSERVER_FILEOPERATIONBASE_H
