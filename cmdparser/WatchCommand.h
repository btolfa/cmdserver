/** @brief Класс реализующий комманду Watch
 *
 */

#pragma once
#ifndef CMDSERVER_WATCHCOMMAND_H
#define CMDSERVER_WATCHCOMMAND_H

#include "Command.h"
#include "ParserException.h"

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace cmdserver {

struct WatchCommand : public Command {
    WatchCommand(std::string const& cmdline) : path(cmdline.substr(6)) {
        if ((! fs::exists(path)) && (! fs::exists(fs::canonical(path).parent_path()))) {
            throw ParserException("No such file or directory");
        }
    }

    void operator()(std::shared_ptr<CommandOutput> const& p_output) override {
        p_output->add_record(path);
    }

    fs::path path;
};

}


#endif //CMDSERVER_WATCHCOMMAND_H
