/** @brief Класс исключительной ситуации во время парсинга комманд
 *
 */

#pragma once
#ifndef CMDSERVER_PARSEREXCEPTION_H
#define CMDSERVER_PARSEREXCEPTION_H

#include <stdexcept>

namespace cmdserver {

class ParserException : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

}

#endif //CMDSERVER_PARSEREXCEPTION_H
