/** @brief Общий интерфейс для всех комманд
 *
 */

#pragma once
#ifndef CMDSERVER_COMMAND_H
#define CMDSERVER_COMMAND_H

#include <memory>
#include "../server/CommandOutput.h"

namespace cmdserver {

struct Command {
    virtual ~Command() = default;

    virtual void operator()(std::shared_ptr<CommandOutput> const&) = 0;
};

}

#endif //CMDSERVER_COMMAND_H
