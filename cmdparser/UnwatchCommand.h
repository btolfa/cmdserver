/** @brief Класс реализующий команду Unwatch
 *
 */

#pragma once
#ifndef CMDSERVER_UNWATCHCOMMAND_H
#define CMDSERVER_UNWATCHCOMMAND_H

#include "Command.h"

#include "ParserException.h"

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace cmdserver {

struct UnwatchCommand : public Command {
    UnwatchCommand(std::string const& cmdline) : path(cmdline.substr(8)) {

    }

    void operator()(std::shared_ptr<CommandOutput> const& p_output) override {
        p_output->remove_record(path);
    }

    fs::path path;
};

}


#endif //CMDSERVER_UNWATCHCOMMAND_H
