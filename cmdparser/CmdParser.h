/** @brief Класс реализующий парсер входящих комманд
 *
 * Должен поддерживать команды:
 * ls path/to/dirname - директории и обычные файлы
 *      для директорий выводит содержимое директории
 *      для файлов выводит имя файла
 * cat path/to/file - только обычные файлы
 *      выводит содержимое файла
 * Watch /path/to/file - только файл
 *      Начинает слежение за файлом, при наступлении событий отправляет уведомление
 * Unwatch /path/to/file - только файл
 *      Прекращает слежение за файлом, если оно было начато с помощью команды Watch
 */

#pragma once
#ifndef CMDSERVER_CMDPARSER_H
#define CMDSERVER_CMDPARSER_H

#include <memory>
#include <string>

#include <boost/algorithm/string/predicate.hpp>

#include "Command.h"
#include "ListCommand.h"
#include "CatCommand.h"
#include "WatchCommand.h"
#include "UnwatchCommand.h"
#include "ParserException.h"

namespace cmdserver {

class CmdParser {
public:
    static std::unique_ptr<Command> parse(std::string const& cmdline) {
        if (boost::starts_with(cmdline, "ls ")) {
            return std::make_unique<ListCommand>(cmdline);
        }

        if (boost::starts_with(cmdline, "cat ")) {
            return std::make_unique<CatCommand>(cmdline);
        }

        if (boost::starts_with(cmdline, "Watch ")) {
            return std::make_unique<WatchCommand>(cmdline);
        }

        if (boost::starts_with(cmdline, "Unwatch ")) {
            return std::make_unique<UnwatchCommand>(cmdline);
        }

        throw ParserException("Unknown command");
    }
};

}

#endif //CMDSERVER_CMDPARSER_H
