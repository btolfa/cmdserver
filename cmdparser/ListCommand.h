/** @brief Класс реализующий комманду ls
 *
 */

#pragma once
#ifndef CMDSERVER_LISTCOMMAND_H
#define CMDSERVER_LISTCOMMAND_H

#include <string>
#include <sstream>

#include "Command.h"
#include "ParserException.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

namespace cmdserver {

struct ListCommand : public Command {
    ListCommand(std::string const& cmdline) : path(cmdline.substr(3)) {
        if (! fs::exists(path)) {
            throw ParserException("No such file or directory");
        }
    }

    void operator()(std::shared_ptr<CommandOutput> const& p_output) override {
        if (fs::is_directory(path)) {
            std::ostringstream os;
            std::for_each(fs::directory_iterator(path), fs::directory_iterator(), [&os](fs::directory_entry &it){
                os << it.path().filename().string() << "\n";
            });
            p_output->write(os.str());
        } else {
            p_output->write(path.filename().string() + "\n");
        }
    }

    fs::path path;
};

}

#endif //CMDSERVER_LISTCOMMAND_H
