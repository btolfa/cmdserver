/** @brief Класс реализующий комманду cat
 *
 */

#pragma once
#ifndef CMDSERVER_CATCOMMAND_H
#define CMDSERVER_CATCOMMAND_H

#include "Command.h"
#include "ParserException.h"


#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
namespace fs = boost::filesystem;

namespace cmdserver {

struct CatCommand : public Command {
    CatCommand(std::string const& cmdline) : path(cmdline.substr(4)) {
        if (! fs::exists(path)) {
            throw ParserException{"No such file or directory"};
        } else if (fs::is_directory(path)) {
            throw ParserException{"Is a directory"};
        } else if (fs::is_other(path)) {
            throw ParserException{"Is a other"};
        }
    }

    void operator()(std::shared_ptr<CommandOutput> const& p_output) override {
        fs::ifstream ifs{path};
        p_output->write(std::string(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>()));
    }

    fs::path path;
};

}

#endif //CMDSERVER_CATCOMMAND_H
