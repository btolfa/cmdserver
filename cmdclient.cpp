/** Приложение клиент для работы с cmdserver
 *
 * @author Tengiz Sharafiev <btolfa@gmail.com>
 * @date 14.05.2015
*/

#include <thread>
#include <boost/asio.hpp>

#include "client/TcpClient.h"

extern "C" {
    #include "linenoise/linenoise.h"
}

int main() {
    boost::asio::io_service ios;
    std::uint16_t port = 20000;

    cmdserver::TcpClient client{ios, "127.0.0.1", port};
    client.register_on_connect([&ios]{
        std::cout << "[TcpClient] Connected!" << std::endl;
    });
    client.do_connect();

    std::thread network([&ios]{ios.run();});

    char * line;
    while ((line = ::linenoise("> ")) != nullptr) {
        if (line[0] != '\0') {
            client.send(line);
        }
        ::free(line);
    }

    network.join();
    return 0;
}